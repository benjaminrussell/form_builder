module [wrapContent]

html = \string -> string
css = \string -> string
js = \string -> string

wrapContent : List U8 -> List U8
wrapContent = \content ->

    htmlContent =
        html
            """
            <html lang="en">
              <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <title>Pointing Poker</title>
                <link rel="stylesheet" href="./main.css">
                <script src="https://unpkg.com/htmx.org@1.9.12"></script>
                <script src="https://unpkg.com/htmx.org@1.9.12/dist/ext/sse.js"></script>
              </head>

                <body hx-boost="true">
                    <header></header>
                    <main>
                        <div class="route_view">
                            <div id="form_text">
                                <h1 id="form_title">Welcome Jason</h1>
                                <span id="form_info">As the first step in your process with 
                                us we ask that you setup a secure passwrod and establish 
                                security questions so that we can confirm you identity in 
                                the future. Once you set you security information below 
                                you will be redirected to you activites page.</span>
                            </div>
                            <div class="form_grid">
                                $(parseContent content)
                            </div>
                            <div id="after_grid">
                            <hr>
                            <p>Please review this website’s Terms & Conditions below to understand how your personal information is processed and if you accept them, please check the box next to Accept Our Terms & Conditions. In addition, once you've reviewed our Privacy Policy, to understand how your personal information is processed, please check the box next to Review Our Policy and click Accept to continue.</p>
                            <div class="after_checkbox">
                                <label class=\"input\"><span><div class=\"inline\"><input type="checkbox"/><span class="input_info">Accept our terms and conditions</span></div></label>
                                <label class=\"input\"><span><div class=\"inline\"><input type="checkbox"/><span class="input_info">Review our privacy policy</span></div></label>
                            </div>
                            <div class="toggle_btns">
                            <button class="btn btn_one">Decline</button>
                            <button class="btn btn_two">Accept</button>
                            </div>
                            <button class="btn">Submit</button>
                            </div>
                        </div>
                    </main>
                    <footer></footer>
                </body>
            </html>
            """

    cssContent =
        css
            """
            body {
                display: grid;
                grid-template-areas: 
                "header"
                "main"
                "footer";
                grid-template-rows: 50px 1fr 50px;
                margin: 0px;
            }
            header {
                grid-area: header;
            }
            main {
                grid-area: main;
            }
            footer {
                grid-area: footer;
            }
            """

    jsContent = ""

    view = { html: htmlContent, css: cssContent, js: jsContent }
    makePage view

parseContent : List U8 -> Str
parseContent = \content ->
    when Str.fromUtf8 content is
        Ok res -> res
        Err _ -> crash "idk man"

makePage : { html : Str, js : Str, css : Str } -> List U8
makePage = \pageParts ->
    html
        """
           $(pageParts.html)

           <script>
               $(pageParts.js)
           </script>

           <style>
               $(pageParts.css)
           </style>

        """
    |> Str.toUtf8


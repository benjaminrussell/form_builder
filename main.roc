app [main] {
    pf: platform "https://github.com/roc-lang/basic-webserver/releases/download/0.4.0/iAiYpbs5zdVB75golcg_YMtgexN3e2fwhsYPLPCeGzk.tar.br",
    json: "https://github.com/lukewilliamboswell/roc-json/releases/download/0.10.0/KbIfTNbxShRX1A1FgXei1SpO5Jn8sgP6HP6PXbi-xyA.tar.br",
}
import pf.Stderr
import pf.Task exposing [Task]
import pf.Http exposing [Request, Response]
import pf.Url exposing [Url]
import json.Json
import json.OptionOrNull exposing [OptionOrNull]
import App exposing [wrapContent]
import Form exposing [buildForm]
import "./main.css" as mainCss : List U8
import "./data.json" as jsonData : List U8

main : Request -> Task Response []
main = \req ->
    handleReq req |> Task.onErr handleErr

handleReq = \req ->
    urlSegments =
        req.url
        |> Url.fromStr
        |> Url.path
        |> Str.split "/"
        |> List.dropFirst 1

    when (req.method, urlSegments) is
        (Get, [""]) -> makeResponse (wrapContent (buildForm (getFormList jsonData)))
        (Get, ["main.css"]) -> makeResponse mainCss
        _ -> Task.err (URLNotFound req.url)

makeResponse : List U8 -> Task Response []_
makeResponse = \bytes ->
    Task.ok {
        status: 200,
        headers: [
            { name: "Cache-Control", value: Str.toUtf8 "max-age=120" },
        ],
        body: bytes,
    }

getFormList = \data ->
    decoder = Json.utf8With { fieldNameMapping: PascalCase }
    decoded = Decode.fromBytesPartial data decoder

    when decoded.result is
        Ok record -> record.inputs
        Err _ -> crash "Json decoding failed"

handleErr : _ -> Task Response []_
handleErr = \err ->
    dbg err

    (msg, code) =
        when err is
            _ -> (Str.joinWith ["SERVER ERROR"] " ", 500)

    {} <- Stderr.line msg |> Task.await

    Task.ok {
        status: code,
        headers: [],
        body: [],
    }


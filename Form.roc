module [buildForm]

FormObj : { label : Str, type : Str, info : Str, required : Bool, value : Str, options : List Str, children : List ChildObj }
ChildObj : { label : Str, type : Str, info : Str, required : Bool, value : Str, options : List Str }

buildForm : List FormObj -> List U8
buildForm = \data ->
    data
    |> List.walk "" \acc, elm -> Str.concat acc (getInput elm)
    |> Str.toUtf8

buildCheckbox = \data ->
    """
    <label class=\"input\"><span>$(data.label)$(requiredMarker data.required)</span><div class=\"inline\"><input type=\"$(data.type)\"/><span class=\"input_info\">$(data.info)</span></div></label>
    """
buildOption = \data ->
    """
    <label class=\"input\"><span>$(data.label)$(requiredMarker data.required)</span><select>$(makeOptions data.options)</select><span class=\"input_info\">$(data.info)</span></label>
    """
buildGroup = \data ->
    """
    <div class="input group">
    <span class=\"group_title\">$(data.label)</span>
    <div class=\"group_items\">
    $(buildChildren data.children)
    </div>
    <span class="group_info">$(data.info)</span>
    </div>
    """
buildText = \data ->
    """
    <label class=\"input\"><span>$(data.label)$(requiredMarker data.required)</span><input type=\"$(data.type)\" value=\" $(data.value)\" /><span class=\"input_info\">$(data.info)</span></label>
    """

buildTitle = \data ->
    """
        <span class=\"title\">$(data.label)</span>
    """

getInput : FormObj -> Str
getInput = \data ->
    if data.type == "checkbox" then
        buildCheckbox data
    else if data.type == "option" then
        buildOption data
    else if data.type == "group" then
        buildGroup data
    else
        buildText data

getChild : ChildObj -> Str
getChild = \data ->
    if data.type == "checkbox" then
        buildCheckbox data
    else if data.type == "option" then
        buildOption data
    else if data.type == "title" then
        buildTitle data
    else
        buildText data

buildChildren : List ChildObj -> Str
buildChildren = \children ->
    children
    |> List.walk "" \acc, elm -> Str.concat acc (getChild elm)

requiredMarker : Bool -> Str
requiredMarker = \isRequired ->
    if isRequired == Bool.true then
        "<span class=\"required_marker\" style=\" color: red\">*</span>"
    else
        ""

makeOptions : List Str -> Str
makeOptions = \options ->
    options
    |> List.walk "" \acc, item ->
        Str.concat acc "<option value=\"$(item)\">$(item)</option>"

parseContent : List U8 -> Str
parseContent = \content ->
    when Str.fromUtf8 content is
        Ok res -> res
        Err _ -> crash "idk man"
